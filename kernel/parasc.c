#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <linux/wait.h>
#include <linux/ptrace.h>
#include <linux/types.h>
#include <linux/spinlock.h>
#include <asm/syscall.h>

#define sync_val_cas(ptr, oldval, newval)                                      \
	__sync_val_compare_and_swap(ptr, oldval, newval)
#define sync_bool_cas(ptr, oldval, newval)                                     \
	__sync_bool_compare_and_swap(ptr, oldval, newval)

static DEFINE_RWLOCK(shms_lock); // all share_mem lock

static int unused_id = 1;
struct share_mem {
	// struct task_struct;
	int id;
	void *shm_addr;
	unsigned int size;
	wait_queue_head_t waitqueue;
	rwlock_t lock;
};

#define SHM_NUM 10

static struct share_mem init_shm = {
	.id = 0,
	.shm_addr = NULL,
	.size = 0,
};
// リストなどを使用した方がよさそう
static struct share_mem shms[SHM_NUM] = {
		[0 ... SHM_NUM - 1] =
			{
				.id = 0,
				.shm_addr = NULL,
				.size = 0,
				/* .waitqueue = , // ここで wait_queue を初期化したい */
				/* .lock = ,  // 上に同じく */
			},
};

/* struct shm_struct { */
/* 	unsigned char status; */
/* 	int request_count; */
/* 	struct syscall_request *requests; */
/* }; */

struct syscall_request {
	unsigned char status;
	unsigned short int sysnum;
	unsigned short int args_num;
	unsigned long args[6];
	long ret_val;
};

/* syscall_page.status */
#define ENTRY_FREE 0
#define ENTRY_REQUESTING 1
#define ENTRY_REQUEST 2
#define ENTRY_PROCESSING 3
#define ENTRY_SUCCESS 4
#define ENTRY_ERROR 5

/*
 *  return index of shms[]
 */
static int find_index(int id)
{
	int index;
	int i;

	for (i = 0; i < SHM_NUM; i++)
		if (shms[i].id == id)
			break;

	return index = i < SHM_NUM ? i : -1;
}

static int exist_request(int *req_count_p, wait_queue_head_t *q, rwlock_t *lock)
{
	read_lock(lock);

	if (*req_count_p > 0) { // exist

		read_unlock(lock);
		return 1;

	} else { // none
		wait_queue_entry_t wait;

		init_waitqueue_entry(&wait, current);
		__set_current_state(TASK_INTERRUPTIBLE);
		add_wait_queue(q, &wait);
		read_unlock(lock);
		schedule();
		remove_wait_queue(q, &wait);
		return 0;
	}
}

static void do_parasc(int index, struct syscall_request *head, int *req_count_p,
		      const unsigned int req_num)
{
	struct syscall_request *request_p;
	int i;
	int old, new;

	while (exist_request(req_count_p, &shms[index].waitqueue,
			     &shms[index].lock)) {
		request_p = head;

		for (i = 0; i < req_num; i++) {
			if (sync_val_cas(&request_p->status, ENTRY_REQUEST,
					 ENTRY_PROCESSING) == ENTRY_REQUEST) {
				unsigned long ret;
				struct pt_regs regs;

				regs.di = request_p->args[0];
				regs.si = request_p->args[1];
				regs.dx = request_p->args[2];
				regs.r10 = request_p->args[3];
				regs.r8 = request_p->args[4];
				regs.r9 = request_p->args[5];
				ret = sys_call_table[request_p->sysnum](&regs);

				request_p->ret_val = ret;
				request_p->status = ENTRY_SUCCESS;

				do {
					old = *req_count_p;
					new = old - 1;
				} while (!sync_bool_cas(req_count_p, old, new));
			}

			request_p++;
		}
	}
}

/* parasc_entry */
SYSCALL_DEFINE2(parasc_entry, void __user *, mem_p, unsigned int, shm_size)
{
	int id = 0;
	int i;

	write_lock(&shms_lock);
	for (i = 0; i < SHM_NUM; i++) { // find id matching for memory address
		if (shms[i].shm_addr == mem_p) {
			write_unlock(&shms_lock);
			return shms[i].id;
		}
	}

	for (i = 0; i < SHM_NUM; i++) { // serching unused element of shms[]
		if (shms[i].id == 0) {
			int old, new;
			id = unused_id;
			do {
				old = unused_id;
				new = old + 1;
			} while (!sync_bool_cas(&unused_id, old, new));
			break;
		}
	}

	if (i >= SHM_NUM) { // filled shms
		write_unlock(&shms_lock);
		return -1;
	}

	shms[i].id = id;
	shms[i].shm_addr = mem_p;
	shms[i].size = shm_size;
	rwlock_init(&shms[i].lock);
	init_waitqueue_head(&shms[i].waitqueue);

	write_unlock(&shms_lock);
	return id;
}

/* parasc_enter */
SYSCALL_DEFINE1(parasc_enter, int, id)
{
	int index;
	int req_num_max;
	void *p;
	unsigned char *exit_flag_p;
	int *req_count_p;
	struct syscall_request *head;

	read_lock(&shms_lock);
	index = find_index(id);
	if (index < 0) {
		read_unlock(&shms_lock);
		return -1;
	}

	p = shms[index].shm_addr;
	req_num_max = (shms[index].size - sizeof(unsigned char) - sizeof(int)) /
		      sizeof(struct syscall_request);

	read_unlock(&shms_lock);

	exit_flag_p = (unsigned char *)p;
	p = (unsigned char *)p + 1;
	req_count_p = (int *)p;
	p = (int *)p + 1;
	head = (struct syscall_request *)p;

	/* TODO: write lock */
	*exit_flag_p = 0;
	while (*exit_flag_p != 1) { /* TODO(?): read_lock */
		do_parasc(index, head, req_count_p, req_num_max);
	}

	return 0;
}

/* parasc_notify */
SYSCALL_DEFINE1(parasc_notify, int, id)
{
	int index;
	read_lock(&shms_lock);
	index = find_index(id);
	if (index < 0) {
		read_unlock(&shms_lock);
		return -1;
	}
	read_unlock(&shms_lock);

	read_lock(&shms[index].lock);
	wake_up_interruptible_all(&shms[index].waitqueue);
	read_unlock(&shms[index].lock);
	return 0;
}

/* parasc_exit */
SYSCALL_DEFINE1(parasc_exit, int, id)
{
	unsigned char *exit_flag_p;
	int index;

	/* set a exit_flag */
	write_lock(&shms_lock);
	index = find_index(id);
	if (index < 0) {
		write_unlock(&shms_lock);
		return -1;
	}
	exit_flag_p = (unsigned char *)shms[index].shm_addr;
	*exit_flag_p = 1;
	/* wake up and initialize */
	wake_up_interruptible_all(&shms[index].waitqueue);
	shms[index] = init_shm;
	write_unlock(&shms_lock);

	return 0;
}
