/*  */
#include <unistd.h>
#include <sys/syscall.h>
#include <pthread.h>
#include <sys/mman.h>

#include "parasc.h"

/* debug */
#include <stdio.h>
#include <time.h>

struct timespec tsStart, tsEnd;
int nsec, secSpan;
/* timespec_get(&tsStart, TIME_UTC); */

/* ----測定する処理---- */

/* timespec_get(&tsEnd, TIME_UTC); */
/* nsec = tsEnd.tv_nsec - tsStart.tv_nsec; */
/* secSpan = tsEnd.tv_sec - tsStart.tv_sec; */
/* if (0 < secSpan) { */
/*   nsec += secSpan * 1000000000; */
/*  } */
/* printf("%d ns\n", nsec); */

#define SHM_NUM 10
struct id_list {
	int id;
	void *addr;
	unsigned int size;
	int req_num_max;
  int *req_count_p;
  struct syscall_request *requests;
	pthread_t thread;
  
};

static struct id_list init_shm = {
	.id = 0,
	.addr = NULL,
	.size = 0,
	.req_num_max = 0,
	.req_count_p = NULL,
	.requests = NULL,
};

static struct id_list shms[SHM_NUM] = {
		[0 ... SHM_NUM - 1] =
			{
				.id = 0,
				.addr = NULL,
				.size = 0,
				.req_num_max = 0,
				.req_count_p = NULL,
				.requests = NULL,
			},
};

static int find_index(int id)
{
	int i;

	for (i = 0; i < SHM_NUM; i++)
		if (shms[i].id == id)
			break;

	return i < SHM_NUM ? i : -1;
}

/*
 *
 */
void *parasc_mmap(unsigned int size)
{
	void *addr;
	addr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_ANON | MAP_SHARED,
		    -1, 0);
	return addr;
}

/*
 *
 */
void parasc_munmap(int id)
{
	int index;
	index = find_index(id);
	munmap(shms[index].addr, shms[index].size);
}

/*
 *
 */
int parasc_entry(void *addr, unsigned int size)
{
	int i;
	int index;
	int id;
	void *p = addr;
	unsigned char *exit_flag_p;
	int *req_count_p;
	struct syscall_request *requests;

	exit_flag_p = (unsigned char *)p;
	p = (unsigned char *)p + 1;
	req_count_p = (int *)p;
	p = (int *)p + 1;
	requests = (struct syscall_request *)p;

	id = syscall(SYS_parasc_entry, addr, size);

	/* id が既にある */
	for (i = 0; i < SHM_NUM; i++) {
		if (shms[i].id == id)
			return id;
	}

	for (i = 0; i < SHM_NUM; i++) {
		if (shms[i].id == 0) {
			shms[i].id = id;
			shms[i].addr = addr;
			shms[i].req_count_p = req_count_p;
			shms[i].requests = requests;
			shms[i].size = size;
			shms[i].req_num_max = REQ_NUM_MAX(size);
			index = i;
			break;
		}
	}

	*exit_flag_p = 0;
	*req_count_p = 0;
	for (i = 0; i < shms[index].req_num_max; i++) {
		/* printf("req_p: %p\n", &req_p[i]); */
		requests[i].status = ENTRY_FREE;
	}

	return id;
}

static void *pthread_parasc_enter(void *args)
{
	int *id = (int *)args;
	syscall(SYS_parasc_enter, *id);
	return 0;
}

/*
 *
 */
void parasc_enter(int id)
{
	int index;
	index = find_index(id);
	pthread_create(&shms[index].thread, NULL, pthread_parasc_enter,
		       (void *)&shms[index].id);
}

/*
 *
 */
void parasc_exit(int id)
{
	int index;
	index = find_index(id);

	// TODO: カーネル側が寝ていることが保証されていなければならない
	//       = リクエストが全て処理し終わっている
	usleep(100); // 代替処理
	

	syscall(SYS_parasc_exit, id);

	shms[index] = init_shm;
	pthread_join(shms[index].thread, NULL);
}

/*
 *
 */
void parasc_notify(int id)
{
	syscall(SYS_parasc_notify, id);
}

static int __parasc_setN(int id, unsigned short int req_num,
			 unsigned short int sysnum, unsigned long arg1,
			 unsigned long arg2, unsigned long arg3,
			 unsigned long arg4, unsigned long arg5,
			 unsigned long arg6)
{
	int index;
	/* void *p; */
	int *request_count;
	struct syscall_request *requests;

	index = find_index(id);
	request_count = shms[index].req_count_p;
	requests = shms[index].requests;
	/* p = shms[index].addr; */

	/* p = (unsigned char *)p + 1; */
	/* request_count = (int *)p; */
	/* p = (int *)p + 1; */
	/* requests = (struct syscall_request *)p; */

	if (requests[req_num].status == ENTRY_FREE) {
		int old, new;

		do {
			old = *request_count;
			new = old + 1;
		} while (!sync_bool_cas(request_count, old, new));

		requests[req_num].status = ENTRY_REQUESTING;
		requests[req_num].sysnum = sysnum;
		requests[req_num].args_num = 0;
		requests[req_num].ret_val = -1;

		requests[req_num].args[0] = (unsigned long)arg1;
		requests[req_num].args[1] = (unsigned long)arg2;
		requests[req_num].args[2] = (unsigned long)arg3;
		requests[req_num].args[3] = (unsigned long)arg4;
		requests[req_num].args[4] = (unsigned long)arg5;
		requests[req_num].args[5] = (unsigned long)arg6;

		requests[req_num].status = ENTRY_REQUEST;
	} else {
		return -1;
	}

	return 0;
}

int parasc_set0(int id, unsigned short int req_num, unsigned short int sysnum)
{
	return __parasc_setN(id, req_num, sysnum, 0, 0, 0, 0, 0, 0);
}

int parasc_set1(int id, unsigned short int req_num, unsigned short int sysnum,
		unsigned long args1)
{
	return __parasc_setN(id, req_num, sysnum, args1, 0, 0, 0, 0, 0);
}

int parasc_set2(int id, unsigned short int req_num, unsigned short int sysnum,
		unsigned long args1, unsigned long args2)
{
	return __parasc_setN(id, req_num, sysnum, args1, args2, 0, 0, 0, 0);
}

int parasc_set3(int id, unsigned short int req_num, unsigned short int sysnum,
		unsigned long args1, unsigned long args2, unsigned long args3)
{
	return __parasc_setN(id, req_num, sysnum, args1, args2, args3, 0, 0, 0);
}
int parasc_set4(int id, unsigned short int req_num, unsigned short int sysnum,
		unsigned long args1, unsigned long args2, unsigned long args3,
		unsigned long args4)
{
	return __parasc_setN(id, req_num, sysnum, args1, args2, args3, args4, 0,
			     0);
}
int parasc_set5(int id, unsigned short int req_num, unsigned short int sysnum,
		unsigned long args1, unsigned long args2, unsigned long args3,
		unsigned long args4, unsigned long args5)
{
	return __parasc_setN(id, req_num, sysnum, args1, args2, args4, args5,
			     args5, 0);
}

int parasc_set6(int id, unsigned short int req_num, unsigned short int sysnum,
		unsigned long args1, unsigned long args2, unsigned long args3,
		unsigned long args4, unsigned long args5, unsigned long args6)
{
	return __parasc_setN(id, req_num, sysnum, args1, args2, args3, args4,
			     args5, args6);
}

int get_status(int id, unsigned short int req_num)
{
	int index;
	/* void *p; */
	struct syscall_request *requests;

	index = find_index(id);
	requests = shms[index].requests;
	/* p = shms[index].addr; */

	/* p = (unsigned char *)p + 1; */
	/* p = (int *)p + 1; */
	/* requests = (struct syscall_request *)p; */
	

	return requests[req_num].status;
}

int set_status(int id, unsigned short int req_num, unsigned char status)
{
	int index;
	/* void *p; */
	struct syscall_request *requests;

	index = find_index(id);
	requests = shms[index].requests;
	/* p = shms[index].addr; */

	/* p = (unsigned char *)p + 1; */
	/* p = (int *)p + 1; */
	/* requests = (struct syscall_request *)p; */

	requests[req_num].status = status;
	return 0;
}

int get_result(int id, unsigned short int req_num)
{
	int index;
	/* void *p; */
	struct syscall_request *requests;

	index = find_index(id);
	requests = shms[index].requests;
	/* p = shms[index].addr; */

	/* p = (unsigned char *)p + 1; */
	/* p = (int *)p + 1; */
	/* requests = (struct syscall_request *)p; */

	return requests[req_num].ret_val;
}
