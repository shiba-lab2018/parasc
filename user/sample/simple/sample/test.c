#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <stdatomic.h>


#define sync_bool_cas(ptr, oldval, newval)		\
  __sync_bool_compare_and_swap(ptr, oldval, newval)

/* 変更可能 */
#define SHM_SIZE 4096
#define THREAD_NUM 1

#define SYS_parasc_entry 335
#define SYS_parasc_enter 336
#define SYS_parasc_notify 337
#define SYS_parasc_exit 338

struct syscall_page {
  unsigned char status;
  unsigned short int sysnum;
  unsigned short int args_num;
  unsigned long args[6];
  long ret_val;
};

#define ENTRY_FREE        0
#define ENTRY_REQUESTING  1
#define ENTRY_REQUEST     2
#define ENTRY_PROCESSING  3
#define ENTRY_SUCCESS     4
#define ENTRY_ERROR       5


struct thread_args {
  unsigned int shm_size;
  void *shm_p;
  int id;
};


void *pthread_parasc_enter(void *p){
  int ret;
  struct thread_args *args = (struct thread_args *)p;
      
  ret =  syscall(SYS_parasc_enter, args->id);
  if(ret < 0){
    printf("error: return_value: %d\n", ret);
    return 0;
  }
  printf("exit parasc_enter\n");

  return 0;
}

int main( int argc, char *argv[ ] )
{
  unsigned int i;
  int id[10];
  struct timespec tsStart, tsEnd;
  int nsec, secSpan;
  
  /* thread variables */
  pthread_t th[THREAD_NUM];
  struct thread_args th_args = {
    .shm_size = SHM_SIZE,
    .shm_p = NULL,
  };
    
  /* initialize shared memory structure */
  void *addr, *shm_p;
  unsigned char *exit_flag_p;
  int *req_count_p;
  struct syscall_page *head, *page_p;
  const unsigned int req_num_max = (SHM_SIZE - sizeof(unsigned char) - sizeof(int)) / sizeof(struct syscall_page);
  //const unsigned int req_num_max = 1;
  const int loop_num = 100;
    
  //struct syscall_page *tmp_p;


  
  addr = mmap(NULL, SHM_SIZE, PROT_READ|PROT_WRITE, MAP_ANON|MAP_SHARED, -1, 0);
  memset(addr, 0, SHM_SIZE);
  id[0] = syscall(SYS_parasc_entry, addr, SHM_SIZE);
  shm_p = addr;
  printf("addr: %p\n", shm_p);
  printf("id: %u\n", id[0]);
  exit_flag_p = (unsigned char *)shm_p;
  shm_p = (unsigned char *)shm_p + 1;
  req_count_p = (int *)shm_p;
  shm_p = (int *)shm_p + 1;
  head = (struct syscall_page *)shm_p;

  *exit_flag_p = 0;
  *req_count_p = 0;
  page_p = head;
  page_p->status = ENTRY_FREE;
  for(i = 0; i < req_num_max; ++i){
    page_p->status = ENTRY_FREE;
    ++page_p;
  }

  // time measurement
  timespec_get(&tsStart, TIME_UTC);
  for(i = 0; i < req_num_max * loop_num; i++){
    syscall(SYS_gettid);
  }
  timespec_get(&tsEnd, TIME_UTC);
  nsec = tsEnd.tv_nsec - tsStart.tv_nsec;
  secSpan = tsEnd.tv_sec - tsStart.tv_sec;
  if (0 < secSpan) {
    nsec += secSpan * 1000000000;
  }
  printf("%d ns\n", nsec);

  
  /*
    create processing thread (parasc_enter(2)) 
  */
  th_args.shm_p = addr;
  th_args.id = id[0];
  for(i = 0; i < THREAD_NUM; i++) {
    pthread_create(&th[i], NULL, pthread_parasc_enter, (void *)&th_args);
  }

  
  /* 処理スレッドが準備できるのを待つ */
  sleep(1);
  
  /* 
     request system calls 
  */
  timespec_get(&tsStart, TIME_UTC);
  int loop;
  for(loop = 0; loop < loop_num; loop++){
    /* printf("req  = %d\n",  *req_count_p); */
    /* printf("loop = %d\n", loop); */
    // gettid(2)
    int count = 0;

    page_p = head;
    for(i = 0; i < req_num_max; ++i){
      if(page_p->status == ENTRY_FREE){
	int old, new;
	count++;
	do {
	  old = *req_count_p;
	  new = old + 1;
	}while(!sync_bool_cas(req_count_p, old, new));
      
	page_p->status = ENTRY_REQUESTING;
	page_p->sysnum = __NR_gettid;
	page_p->args_num = 0;
	page_p->ret_val = -1;
      
	page_p->args[0] = (unsigned long)0;
	page_p->args[1] = (unsigned long)0;
	page_p->args[2] = (unsigned long)0;
	page_p->args[3] = (unsigned long)0;
	page_p->args[4] = (unsigned long)0;
	page_p->args[5] = (unsigned long)0;
      
      
	page_p->status = ENTRY_REQUEST;

	/* break; // test */
      }
    
      page_p++;
    }
    /* timespec_get(&tsEnd, TIME_UTC); */
    /* nsec = tsEnd.tv_nsec - tsStart.tv_nsec; */
    /* secSpan = tsEnd.tv_sec - tsStart.tv_sec; */
    /* if (0 < secSpan) { */
    /*   nsec += secSpan * 1000000000; */
    /* } */
    /* printf("%d ns\n", nsec); */

    syscall(SYS_parasc_notify, id[0]);

    /* timespec_get(&tsEnd, TIME_UTC); */
    /* nsec = tsEnd.tv_nsec - tsStart.tv_nsec; */
    /* secSpan = tsEnd.tv_sec - tsStart.tv_sec; */
    /* if (0 < secSpan) { */
    /*   nsec += secSpan * 1000000000; */
    /* } */
    /* printf("%d ns\n", nsec); */

    /*
      get return value 
    */
    while(count > 0){
      
      page_p = head;
      for(i = 0; i < req_num_max; ++i){
	/* if(page_p->status != ENTRY_FREE && page_p->status != ENTRY_REQUEST && */
	/*    page_p->status != ENTRY_PROCESSING){ */
	if(page_p->status == ENTRY_SUCCESS){
	  count--;
	  /* printf("ret_val: %ld\n", page_p->ret_val); */
	  page_p->status = ENTRY_FREE;
	  break;
	}
	page_p++;
      }
      //usleep(1000); //
    }

  }
  timespec_get(&tsEnd, TIME_UTC);
  nsec = tsEnd.tv_nsec - tsStart.tv_nsec;
  secSpan = tsEnd.tv_sec - tsStart.tv_sec;
  if (0 < secSpan) {
    nsec += secSpan * 1000000000;
  }
  printf("%d ns\n", nsec);
  printf("req_count: %d\n", *req_count_p);
  /* 
     exit parasc
  */
  page_p = head;
  for(i = 0; i < req_num_max; ++i){
    if(page_p->status != ENTRY_FREE){
      printf("status is not FREE\n");
      return -1;
    }
    page_p++;
  }
  for(i = 0; i < 1; i++) {
    syscall(SYS_parasc_exit, id[0]);
  }

  for(i = 0; i < THREAD_NUM; i++) {
    pthread_join(th[i], NULL);  
  }
  munmap(addr, SHM_SIZE);
  return 0;
}
