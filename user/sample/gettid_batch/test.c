#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include "parasc.h"

#define SHM_SIZE 4096*2

#define REQMAX REQ_NUM_MAX(SHM_SIZE)

int main(int argc, char *argv[])
{
  int id;
  int i, j;
  int count = 0;
  void *addr;
  int l;
  struct timespec tsStart, tsEnd;
  int nsec, secSpan;
  int batch_size, n;
  int remain, loop, req_sum = 0;

  /* if(argc < 3){ */
  /*   printf("ARGS: batch_size number_of_requests\n"); */
  /*   return 0; */
  /* } */

  /* // TODO: 引数を int に変換 */
  /* batch_size = atoi(argv[1]); */
  /* n = atoi(argv[2]); */
  /* loop = n / batch_size; */

  /* // TODO: batch_size が REQMAX を超えてはいけない */
  /* if(batch_size > REQMAX) { */
  /*   printf("error: batch_size is too large\n"); */
  /*   return 0; */
  /* } */
  
  for (j = 0; j < 64; j++)  {
    req_sum = 0;
    count = 0;
    
    batch_size = j == 0 ? 1 : j;
    n = 127;
    loop = n / batch_size;

    addr = (struct shm_struct *)parasc_mmap(SHM_SIZE);

    id = parasc_entry(addr, SHM_SIZE);
    /* printf("id: %d\n", id); */
    /* printf("REQMAX: %ld\n", REQMAX); */
    parasc_enter(id);


    timespec_get(&tsStart, TIME_UTC);
    // TODO: ループ回数 = リクエスト数 / バッチサイズ
    for (l = 0; l < loop; l++){
      // TODO: バッチサイズ分だけループ 
      for (i = 0; i < batch_size; i++) {
	parasc_set_request(id, i, __NR_gettid);
	count++;
      }
    
      req_sum += count;



      parasc_notify(id);
    
      while (count > 0) {
	// TODO: バッチサイズ分だけループ 
	for (i = 0; i < batch_size; i++) {
	  while(1){
	    if (get_status(id, i) == ENTRY_SUCCESS) {
	      count--;
	      set_status(id, i, ENTRY_FREE);
	      break;
	    }
	  }
	}
      }
    }

    // TODO: リクエストが余っていたら処理
    remain = n - req_sum;
    
    if(remain > 0){
      for(i = 0; i < remain; i++){
	parasc_set_request(id, i, __NR_gettid);
	count++;
      }
    
      parasc_notify(id);
    
      while (count > 0) {
	for (i = 0; i < remain; i++) {
	  while(1){
	    if (get_status(id, i) == ENTRY_SUCCESS) {
	      count--;
	      set_status(id, i, ENTRY_FREE);
	      break;
	    }
	  }
	}
      }
    }
  
    timespec_get(&tsEnd, TIME_UTC);
    nsec = tsEnd.tv_nsec - tsStart.tv_nsec;
    secSpan = tsEnd.tv_sec - tsStart.tv_sec;
    if (0 < secSpan) {
      nsec += secSpan * 1000000000;
    }
    printf("%d,%d\n", batch_size, nsec);

    /* printf("parasc_exit\n"); */
  
    parasc_exit(id);

    parasc_munmap(id);

  }
  

  return 0;
}
