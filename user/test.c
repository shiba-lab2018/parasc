#include <stdio.h>
#include <time.h>

#include "parasc.h"

#define SHM_SIZE 4096

#define REQMAX REQ_NUM_MAX(SHM_SIZE)
//#define REQMAX 1

int main(int argc, char *argv[])
{
	int id;
	int i;
	int count = 0;
	void *addr;

	struct timespec tsStart, tsEnd;
	int nsec, secSpan;

	addr = (struct shm_struct *)parasc_mmap(SHM_SIZE);

	id = parasc_entry(addr, SHM_SIZE);
	printf("id: %d\n", id);
	parasc_enter(id);

	timespec_get(&tsStart, TIME_UTC);

	for (i = 0; i < REQMAX; i++) {
		parasc_set_request(id, i, __NR_gettid);
		count++;
	}
	parasc_notify(id);

	while (count > 0) {
		for (i = 0; i < REQMAX; i++) {
			if (get_status(id, i) == ENTRY_SUCCESS) {
				count--;
				set_status(id, i, ENTRY_FREE);
				break;
			}
		}
	}
	timespec_get(&tsEnd, TIME_UTC);
	nsec = tsEnd.tv_nsec - tsStart.tv_nsec;
	secSpan = tsEnd.tv_sec - tsStart.tv_sec;
	if (0 < secSpan) {
		nsec += secSpan * 1000000000;
	}
	printf("%d ns\n", nsec);

	printf("parasc_exit\n");
	parasc_exit(id);

	parasc_munmap(id);

	return 0;
}
