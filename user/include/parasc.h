#ifndef PARASC_H
#define PARASC_H

#include <sys/syscall.h>

/*
 * この実装では，複数のユーザスレッドが一つの共有メモリに対して，リク
 * エストを要求したり，結果を受け取ったりすることができない．ユーザス
 * レッド同士が同じ共有メモリを参照する際の競合を解決する必要がある．
 */

#define sync_bool_cas(ptr, oldval, newval)                                     \
	__sync_bool_compare_and_swap(ptr, oldval, newval)
#define sync_val_cas(ptr, oldval, newval)                                      \
	__sync_val_compare_and_swap(ptr, oldval, newval)

#define REQ_NUM_MAX(size)                                                      \
	((size - sizeof(unsigned char) - sizeof(int)) /                        \
	 sizeof(struct syscall_request))

/* system call number */
#define SYS_parasc_entry 335
#define SYS_parasc_enter 336
#define SYS_parasc_notify 337
#define SYS_parasc_exit 338

/* syscall_request.status */
#define ENTRY_FREE 0
#define ENTRY_REQUESTING 1
#define ENTRY_REQUEST 2
#define ENTRY_PROCESSING 3
#define ENTRY_SUCCESS 4
#define ENTRY_ERROR 5

struct shm_struct {
	unsigned char exit_flag;
	int request_count;
	struct syscall_request *req_head;
};

struct syscall_request {
	unsigned char status;
	unsigned short int sysnum;
	unsigned short int args_num;
	unsigned long args[6];
	long ret_val;
};

/**
 *
 */
extern void *parasc_mmap(unsigned int size);

/**
 *
 */
extern void parasc_munmap(int id);

/**
 *
 */
extern int parasc_entry(void *addr, unsigned int size);

/**
 *
 */
extern void parasc_enter(int id);

/**
 *
 */
extern void parasc_exit(int id);

/**
 *
 */
extern void parasc_notify(int id);

#define _parasc_set(_0, _1, _2, _3, _4, _5, _6, x, ...) x

/**
 *
 */
#define parasc_set_request(id, req_num, sysnum, ...)                           \
	_parasc_set(dummy, ##__VA_ARGS__, parasc_set6, parasc_set5,            \
		    parasc_set4, parasc_set3, parasc_set2, parasc_set1,        \
		    parasc_set0)(id, req_num, sysnum, ##__VA_ARGS__)

int parasc_set0(int id, unsigned short int req_num, unsigned short int sysnum);

int parasc_set1(int id, unsigned short int req_num, unsigned short int sysnum,
		unsigned long args1);

int parasc_set2(int id, unsigned short int req_num, unsigned short int sysnum,
		unsigned long args1, unsigned long args2);

int parasc_set3(int id, unsigned short int req_num, unsigned short int sysnum,
		unsigned long args1, unsigned long args2, unsigned long args3);

int parasc_set4(int id, unsigned short int req_num, unsigned short int sysnum,
		unsigned long args1, unsigned long args2, unsigned long args3,
		unsigned long args4);

int parasc_set5(int id, unsigned short int req_num, unsigned short int sysnum,
		unsigned long args1, unsigned long args2, unsigned long args3,
		unsigned long args4, unsigned long args5);

int parasc_set6(int id, unsigned short int req_num, unsigned short int sysnum,
		unsigned long args1, unsigned long args2, unsigned long args3,
		unsigned long args4, unsigned long args5, unsigned long args6);

/**
 *
 */
int get_result(int id, unsigned short int req_num);

/**
 *
 */
int get_status(int id, unsigned short int req_num);

/**
 *
 */
int set_status(int id, unsigned short int req_num, unsigned char status);

#endif
